<?php

namespace Faker\Romania;

use Faker\Extension\Extension;

class PhoneNumber extends \Faker\Provider\PhoneNumber implements Extension
{
    protected static $formats = [
        '{{landlinePhoneNumber}}',
        '{{mobilePhoneNumber}}',
    ];

    /**
     * {@link} https://en.wikipedia.org/wiki/Telephone_numbers_in_Romania
     */
    protected static $landlineFormats = [
        '021#######', // Bucharest
        '023#######',
        '024#######',
        '025#######',
        '026#######',
        '027#######', // non-geographic
        '031#######', // Bucharest
        '033#######',
        '034#######',
        '035#######',
        '036#######',
        '037#######', // non-geographic
        '+40 021#######', // Bucharest
        '+40 023#######',
        '+40 024#######',
        '+40 025#######',
        '+40 026#######',
        '+40 027#######', // non-geographic
        '+40 031#######', // Bucharest
        '+40 033#######',
        '+40 034#######',
        '+40 035#######',
        '+40 036#######',
        '+40 037#######', // non-geographic
    ];

    /**
     * @see https://github.com/google/libphonenumber/blob/v8.13.27/resources/metadata/40/ranges.csv
     */
    protected static $mobileFormats = [
        '062# ### ###',
        '0640 ### ###',
        '0700 ### ###',
        '0701 ### ###',
        '0702 0## ###',
        '0703 46# ###',
        '0703 47# ###',
        '0703 48# ###',
        '0703 49# ###',
        '0705 ### ###',
        '0710 ### ###',
        '0711 ### ###',
        '0712 ### ###',
        '0713 ### ###',
        '0723 ### ###',
        '0745 ### ###',
        '076# ### ###',
        '077# ### ###',
        '0780 3## ###',
        '0780 4## ###',
        '0780 5## ###',
        '0780 6## ###',
        '0780 7## ###',
        '0780 8## ###',
        '0790 9## ###',
        '0791 9## ###',
        '0792 9## ###',
        '0793 ### ###',
        '+40 062# ### ###',
        '+40 0640 ### ###',
        '+40 0700 ### ###',
        '+40 0701 ### ###',
        '+40 0702 0## ###',
        '+40 0703 46# ###',
        '+40 0703 47# ###',
        '+40 0703 48# ###',
        '+40 0703 49# ###',
        '+40 0705 ### ###',
        '+40 0710 ### ###',
        '+40 0711 ### ###',
        '+40 0712 ### ###',
        '+40 0713 ### ###',
        '+40 0723 ### ###',
        '+40 0745 ### ###',
        '+40 076# ### ###',
        '+40 077# ### ###',
        '+40 0780 3## ###',
        '+40 0780 4## ###',
        '+40 0780 5## ###',
        '+40 0780 6## ###',
        '+40 0780 7## ###',
        '+40 0780 8## ###',
        '+40 0790 9## ###',
        '+40 0791 9## ###',
        '+40 0792 9## ###',
        '+40 0793 ### ###',
    ];

    protected static $tollFreeFormats = [
        '0800######',
        '0801######', // shared-cost numbers
        '0802######', // personal numbering
        '0806######', // virtual cards
        '0807######', // pre-paid cards
        '0870######', // internet dial-up
        '+40 0800######',
        '+40 0801######', // shared-cost numbers
        '+40 0802######', // personal numbering
        '+40 0806######', // virtual cards
        '+40 0807######', // pre-paid cards
        '+40 0870######', // internet dial-up
    ];

    protected static $premiumRateFormats = [
        '0900######',
        '0903######', // financial information
        '0906######', // adult entertainment
        '+40 0900######',
        '+40 0903######', // financial information
        '+40 0906######', // adult entertainment
    ];

    /**
     * @example '573 2462'
     *
     * @return string
     */
    public function landlinePhoneNumber()
    {
        return static::numerify($this->generator->parse(static::randomElement(static::$landlineFormats)));
    }

    /**
     * @example '573 2462'
     *
     * @return string
     */
    public function mobilePhoneNumber()
    {
        return static::numerify($this->generator->parse(static::randomElement(static::$mobileFormats)));
    }

    /**
     * @example '0800 051183'
     *
     * @return string
     */
    public function tollFreePhoneNumber()
    {
        return static::numerify($this->generator->parse(static::randomElement(static::$tollFreeFormats)));
    }

    /**
     * @example '0900 192785'
     *
     * @return string
     */
    public function premiumRatePhoneNumber()
    {
        return static::numerify($this->generator->parse(static::randomElement(static::$premiumRateFormats)));
    }
}
