<?php

namespace Faker\Test\Romania;

use Faker\Generator;
use Faker\Romania\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    public function testCity()
    {
        $city = $this->_faker->city;
        $this->assertSame(true, is_string($city) && $city !== '', 'City name is not a valid string');
    }

    public function testStreet()
    {
        $street = $this->_faker->streetName;
        $this->assertSame(true, is_string($street) && $street !== '', 'Street name is not a valid string');
    }

    public function testPostCode()
    {
        $pattern = '^([986][0-2]|[73][0-3]|[54][0-5]|[21][0-4]|0[1-8])[\-]{0,1}\d{4}$^';

        $postCode = $this->_faker->postCode;
        $this->assertMatchesRegularExpression($pattern, $postCode);
    }
}
