Under development
-----------------------------------------------

1.1.1 2024-01-02
-----------------------------------------------
- Bugfix: Fix mobile phonenumber formats (Kalmer)

1.1.0 2023-08-23
-----------------------------------------------
- Feature: Add phonenumber methods (Kalmer)

1.0.0 2023-08-08
-----------------------------------------------
- Feature: Initial implementation (Siim)
